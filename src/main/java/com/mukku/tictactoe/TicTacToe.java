/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mukku.tictactoe;

import java.util.Scanner;

/**
 *
 * @author acer
 */
public class TicTacToe {

    static char board[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char currPlayer = 'O';
    static int row, col;
    static int countDat = 0;

    static Scanner kb = new Scanner(System.in);

    public static void main(String[] args) {
        showWelcome();
        while (true) {
            showBoard();
            showTurnOX();
            inputRowCol();
            process();
            if (checkEnd()) {
                break;
            }
        }
    }

    private static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    private static void showBoard() {
        for (int r = 0; r < board.length; r++) {
            for (int c = 0; c < board[r].length; c++) {
                System.out.print(board[r][c] + " ");
            }
            System.out.println();
        }
    }

    private static void showTurnOX() {
        System.out.println("Turn " + currPlayer);
    }

    private static void inputRowCol() {
        System.out.println("Please input row, col:");
        row = kb.nextInt();
        col = kb.nextInt();
    }

    private static void process() {
        if (setOnBoard()) {
            if (checkEnd()) {
                showResult();
                return;
            }
            switchCurrPlayer();
        } else {
            inputRowCol();
            process();
        }
    }

    private static void switchCurrPlayer() {
        if (currPlayer == 'O') {
            currPlayer = 'X';
        } else {
            currPlayer = 'O';
        }
    }

    private static boolean setOnBoard() {
        if (row - 1 >= 0 && row - 1 < board.length && col - 1 >= 0 && col - 1 < board.length && board[row - 1][col - 1] == '-') {
            board[row - 1][col - 1] = currPlayer;
            return true;
        }
        return false;
    }

    private static boolean checkEnd() {
        return checkWin() || checkDraw();
    }

    private static boolean checkWin() {
        return checkRow() || checkCol() || checkDiagonal();
    }

    private static boolean checkRow() {
        for (int c = 0; c < board.length; c++) {
            if (board[row - 1][c] != currPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkCol() {
        for (int r = 0; r < board.length; r++) {
            if (board[r][col - 1] != currPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkDiagonal() {
        return checkDia1() || checkDia2();
    }

    private static boolean checkDia1() {
        for (int d = 0; d < board.length; d++) { //00 11 22 <- 11 22 33
            if (board[d][d] != currPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkDia2() {
        for (int d = 0; d < board.length; d++) {
            if (board[d][2 - d] != currPlayer) { //02 11 20 <- 13 22 31
                return false;
            }
        }
        return true;
    }

    private static boolean checkDraw() {
        countDat = 0;
        for (int r = 0; r < board.length; r++) {
            for (int c = 0; c < board[r].length; c++) {
                if ((board[r][c] == '-')) {
                    countDat++;
                }
            }
        }
        if (countDat == 0) {
            return true;
        }
        return false;
    }

    private static void showResult() {
        showBoard();
        if (checkWin()) {
            showWin();
        } else {
            showDraw();
        }
    }

    private static void showWin() {
        System.out.println(">>> " + currPlayer + " Win <<<");
    }

    private static void showDraw() {
        System.out.println(">>> Draw <<<");
    }
}
